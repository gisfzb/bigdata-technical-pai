进入下载页：http://edelivery.oracle.com/osdc/faces/Home.jspx，需要注册账号并且登录，除了邮箱需要填对，因为注册要发链接验证的，其它的一堆信息随便填，我这里已经注册登录好了。

![image-20221129180157545](https://oss.ikeguang.com/image/202211291805847.png)

比如，我这里要下载 Oracle GoldenGate for Big Data 12.3.1.1.1 on Linux x86-64 这个版本

首先，在搜索框里面搜索：

![image-20221129180715569](https://oss.ikeguang.com/image/202211291807803.png)

点击对应搜索结果：

![image-20221129180836101](https://oss.ikeguang.com/image/202211291808666.png)

然后，点击右上角的View Items，就会跳出一个小窗，点击Continue。

![image-20221129181006792](https://oss.ikeguang.com/image/202211291810097.png)

然后，选择系统后，点击Continue按钮。

![image-20221129181038254](https://oss.ikeguang.com/image/202211291810775.png)

勾选同意用户协议，继续点击Continue。

![image-20221129181128527](https://oss.ikeguang.com/image/202211291811217.png)

右边会弹出一个用户调查，不用管它，叉掉就行。

![image-20221129181323445](https://oss.ikeguang.com/image/202211291813769.png)

**当心，不要点击右边的Download按钮，否则会给你下载一个exe文件，我们直接点击V971332-01.zip这个文字链接即可。**

下载后的文件是这样的，刚好是我们需要的版本。

![image-20221129181449015](https://oss.ikeguang.com/image/202211291814172.png)

